from __future__ import division
import os
from numpy import array as ar
import numpy as np
from elo import *

leagues= ['BBl', 'LaLi', 'BZ-Kl', 'A-Kl', 'B-Kl', 'C-Kl', 'D-Kl', 'E-Kl', 'F-Kl', 'G-Kl', 'H-Kl']


class Perspective_game:
    def __init__(self):
        self.heim= True
        self.points= ar((0))
        self.date=np.zeros((3,1))
        self.win= True
        self.player=-1
        self.partner= 0
        self.opponent1= 0
        self.opponent2= 0
        self.klasse='Buchstabensuppe'
        self.elo_pl=0
        self.elo_pa=0
        self.elo_o1=0
        self.elo_o2=0
        self.league_pl=-1 # 1 fuer bbl, dann steigend
        self.league_op=-1

class Mensch:
    def __init__(self, name, idx):
        self.m=[]
        self.s=[]
        self.d=[]
        self.name= name
        self.idx= idx
        
    def calc(self):
        self.s_winrate= 0
        if len(self.s) > 0:
            self.s_winrate= sum([g.win for g in self.s])/len(self.s)
    
    def __str__(self):
        return self.name + ' s_winrate: ' + str(self.s_winrate)
        

def read_data():
    name_list= sorted(os.listdir('data'))
    name_to_idx={}
    idx_to_name={}
    idx_to_mensch={}
    menschen=[]
    for i in range(len(name_list)):
        name_to_idx[name_list[i]]= i
        idx_to_name[i]= name_list[i]
    for name in name_list:
        person= Mensch(name, name_to_idx[name])
        idx_to_mensch[person.idx]= person
        menschen.append(person)
        s_path= 'data/'+name+'/S'
        if os.path.exists(s_path):
            f= open(s_path  ,'r')
            lines= f.readlines()
            s_games=[]
            for line in lines:
                pgame= Perspective_game()
                if not line[35:37]=='  ':
                    #fucking long name
                    buff= line.find(':')
                    if line[buff-2].isdigit():
                        buff-=2
                    else:
                        buff-= 1
                    line= line[:buff]+'  '+line[buff:]
                infos_= line.split('  ')
                infos=[]
                for inf in infos_:
                    if not inf in ['','\n']:
                        infos.append(inf)
                pgame.heim= infos[3]=='heim'
                pgame.klasse= infos[4].strip()
                res=infos[2].split('||')
                res=[set.split(':') for set in res]
                pgame.points= ar([[int(set[0]),int(set[1])] for set in res])
                date= infos[0].split('.')
                pgame.date= ar([int(buff) for buff in date])
                pgame.win= pgame.points[-1,0]>pgame.points[-1,1]
                pgame.opponent1= name_to_idx[infos[1]]
                pgame.player= person.idx
                s_games.append(pgame)
            f.close()
            person.s=s_games
        person.calc()
    #main leagues
    for m in menschen:
        for g in m.s:
            ml_pl= main_league(m, g.date)
            ml_o1= main_league(idx_to_mensch[g.opponent1], g.date)
            g.league_pl= leagues.index(ml_pl)
            g.league_op= leagues.index(ml_o1)
    
    return menschen, idx_to_name, name_to_idx, idx_to_mensch
        
        
        
        
        