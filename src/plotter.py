from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from myreader import *
from oracle import *
from elo import *
from numpy import array as ar
from time import time
from neurnet import *
import pickle

min_num_games= 5
heimvorteil= 40


def relevant_singles(menschen, idx_to_mensch):
    relevant_m=[]
    relevant_games=[]
    for m in menschen:
        if len(m.s) >= min_num_games:
            relevant_m.append(m)
    for m1 in relevant_m:
        for s in m1.s:
            if s.heim:
                m2= idx_to_mensch[s.opponent1]
                if len(m2.s) >= min_num_games:
                    relevant_games.append(s)
    return relevant_games, relevant_m

def recent(games):
    new_games= []
    for g in games:
        if not is_before(g.date, [1,7,2019]):
            new_games.append(g)
    return new_games
    
def plot_neuro_err(f= ""):
#    plt.ion()
#    plt.show()
    if f== "":
        print('reading...')
        buff= time()
        menschen, idx_to_name, name_to_idx, idx_to_mensch= read_data()
        print("this took " , time()-buff)
        print('computing...')
        relevant_games, relevant_m= relevant_singles(menschen, idx_to_mensch)
        elo2= simulate_elo(relevant_m, relevant_games)
        new_games= recent(relevant_games)
        pickle.dump(new_games, open("new_games", "wb"))
    else:
        new_games= pickle.load(open(f, "rb"))
    n= 10
    cases= np.zeros((n,1))
    won= np.zeros((n,1))
    
    prec= 200
    xx=np.arange(0,1.0001, 1/prec)
    c_cont=ar([0.0001 for x in xx])
    w_cont=ar([0 for x in xx])

    #neurozeugs
    net= Net()
    train= True
    if train:
        net.train(new_games, epochs=6000, leorning_rate= .001, perc=.95)
        net.save_weights()
    else:
        net.load_weights()
        net.select_train_set(new_games, perc=.5)
    net.test_games= ar(new_games).reshape((len(new_games),1))
    
    test_games, guess= net.calc_test_data()
     
    guess= ar(guess.detach())
    print('plotting...')
    l= len(xx)
    for g,p in np.concatenate((test_games, guess),1):
#        p= p_x_wins(g.elo_pl+heimvorteil, g.elo_o1)
        i= interval(p,n)
        cases[i]+= 1
        won[i]+= g.win
        #continuous version
        chi_p= ar([1 if abs(x-p)<= .05 else 0 for x in xx ])
        c_cont+= chi_p
        if g.win:
            w_cont+= chi_p
    dens= c_cont*2/len(test_games)
    
    real= w_cont/c_cont
    checksum= sum(c_cont*(real-xx))/sum(c_cont)#checks how much heimvorteil is still left 0 would be ideal
    print("checksum:", checksum)
    heim_v= [(real[i]+real[l-i-1]-1)/2 for i in range(l)]
    plt.plot(xx,xx, color='black')
    plt.plot(xx,[0 for x in xx], color='black')
    plt.plot(xx, real, color= 'red')
#    plt.plot(xx, heim_v, color='blue')
    plt.plot(xx, dens, color='green')
    plt.xlim(0,1)
    plt.show()
    
#    perc= ar([int((1+2*i)/(2*n)*100) for i in range(n)]).reshape((n,1))
#    plt.draw()
#    plt.pause(2.001)

    

def plot_elo_err():
#    plt.ion()
#    plt.show()
    print('reading...')
    buff= time()
    menschen, idx_to_name, name_to_idx, idx_to_mensch= read_data()
    print("this took " , time()-buff)
    print('computing...')
    relevant_games, relevant_m= relevant_singles(menschen, idx_to_mensch)
    elo2= simulate_elo(relevant_m, relevant_games)
    new_games= []
    print('plotting...')
    for g in relevant_games:
        if not is_before(g.date, [1,7,2019]):
            new_games.append(g)
    n= 10
    cases= np.zeros((n,1))
    won= np.zeros((n,1))
    
    prec= 200
    xx=np.arange(0,1.0001, 1/prec)
    c_cont=ar([0 for x in xx])
    w_cont=ar([0 for x in xx])

    l= len(xx)
    for g in new_games:
        p= p_x_wins(g.elo_pl+heimvorteil, g.elo_o1)
        i= interval(p,n)
        cases[i]+= 1
        won[i]+= g.win
        #continuous version
        chi_p= ar([1 if abs(x-p)<= .05 else 0 for x in xx ])
        c_cont+= chi_p
        if g.win:
            w_cont+= chi_p
    dens= c_cont*2/len(new_games)
    
    real= w_cont/c_cont
    checksum= sum(c_cont*(real-xx))/sum(c_cont)
    print("checksum:", checksum)
    heim_v= [(real[i]+real[l-i-1]-1)/2 for i in range(l)]
    plt.plot(xx,xx, color='black')
    plt.plot(xx,[0 for x in xx], color='black')
    plt.plot(xx, real, color= 'red')
#    plt.plot(xx, heim_v, color='blue')
    plt.plot(xx, dens, color='green')
    plt.xlim(0,1)
    plt.show()
    
    perc= ar([int((1+2*i)/(2*n)*100) for i in range(n)]).reshape((n,1))
    table= np.concatenate((perc, cases, won/cases), axis=1)
#    plot_table(table)
#    plt.draw()
#    plt.pause(0.001)

def plot_single_corr():
    menschen, idx_to_name, name_to_idx, idx_to_mensch= read_data()
    
    num_games=np.zeros((11,11))
    num_wins=np.zeros((11,11))
    win_rate=np.zeros((11,11))
    
    relevant_games, relevant_m= relevant_singles(menschen, idx_to_mensch)
    for g in relevant_games:
        rate1= idx_to_mensch[g.player].s_winrate
        rate2= idx_to_mensch[g.opponent1].s_winrate
        i,j= int(rate1*10//1), int(rate2*10//1)
        num_games[i,j]+= 1
        if g.win:
            num_wins[i,j]+= 1
    print(len(relevant_games), "relevant games")
    
    oracle= Oracle()
    table, cases, prediction, reality= oracle.tryout(relevant_games, menschen, idx_to_name, name_to_idx, idx_to_mensch)
#    plot_table(table)

    testsum= sum([cases[i]*reality[i] for i in range(10)])/sum(cases)
    print("testsum:", testsum)
    print(cases.shape, prediction.shape, reality.shape)
    res_table= np.concatenate((cases, prediction, reality), axis=1)
    print(res_table.shape)
    
    plot_table(res_table)
    
    for i in range(11):
        for j in range(11):
            if num_games[i,j] > 0: 
                win_rate[i,j]= num_wins[i,j]/num_games[i,j]
                win_rate[i,j]= round(win_rate[i,j]*100)/100
            else: 
                win_rate[i,j]= np.inf
        
    str_table=[[str(num_games[i,j])+" || "+str(win_rate[i,j]) for j in range(11)] for i in range(11)]
    print("plotting.....")
    
#    plot_table(win_rate[0:10,0:10]-table)
    
    count= 0
    points=[]
    for m in menschen:
        total= len(m.s)
        home= sum([s.heim for s in m.s])
        ausw= sum([not s.heim for s in m.s])
        if home < 4 or ausw <4:
            continue
        else: 
            count+= 1
        home_win= sum([s.heim and s.win for s in m.s])
        ausw_win= sum([(not s.heim) and s.win for s in m.s])
        points.append( [home_win/home, ausw_win/ausw] )
        
        
 
    print(count, " menschen relevant fuer home/ausw-statistik")
#    plt.xlim(0,1)
#    plt.ylim(-1,1)
    xx= np.arange(0,1.01,.01)
#    plt.plot(xx, xx, color='black')
#    plt.plot(xx, [0 for x in xx], color='black')
#    plt.scatter([p[0] for p in points], [p[1] for p in points], color='red')
#    plt.scatter([(p[0]+p[1])/2 for p in points], [p[0]-p[1] for p in points], color='green')
    n= 20
    xx2=np.arange(0,1.000001,1/n)
    yy2=[]
    for i in range(n):
        buff= []
        for p in points:
            if xx2[i]<= (p[0]+p[1])/2 and (p[0]+p[1])/2 < xx2[i+1]:
                buff+=[p[0] - p[1]]
        yy2+=[sum(buff)/len(buff)]
#    plt.plot(xx2[0:-1]+1/(2*n), yy2, color= "blue") 
#    plt.show()
    
    
def plot_table(table):
    dimx, dimy= table.shape
    for i in range(dimx):
        for j in range(dimy):
            try:
                table[i,j]= round(table[i,j]*100)/100
            except:
                pass
    table_pl= table.transpose()
    plt.axis('tight')
    plt.axis('off')
    table_pl=[[table_pl[dimy-i-1, j] for j in range(dimx)] for i in range(dimy)]    
    the_table = plt.table(cellText=table_pl, loc='center')
    the_table.set_fontsize(14)
    plt.show()
    