from __future__ import division
import numpy as np
from numpy import array as ar
from time import localtime, time
import operator

leagues= ['BBl', 'LaLi', 'BZ-Kl', 'A-Kl', 'B-Kl', 'C-Kl', 'D-Kl', 'E-Kl', 'F-Kl', 'G-Kl', 'H-Kl']

def is_before(date1, date2):
    if date1[2]==date2[2]:
        if date1[1]==date2[1]:
            return date1[0]<date2[0]
        else:
            return date1[1]<date2[1]
    else:
        return date1[2]<date2[2]

def total_days(g):
    return g.date[0]+30*g.date[1]+365*g.date[2]

#date ist in for [tag,monat,jahr]
def time_fac(date):
    today= localtime()
    dt= 365* (today[0]-date[2]) + 30*(today[1]-date[1]) + today[2]-date[1]
    return np.exp(-0.8*dt/365)

def p_x_wins(ex, ey):
    return 1/(1+10**((ey-ex)/400))

def step_size(n):
    return 20*.8**n

def main_league(m, date= [1,1,3000]):
    count= {l: 0 for l in leagues}
    for g in m.s:
        if is_before(g.date, date):
            count[g.klasse]+= 1#time_fac(g.date)
    return max(count, key=count.get)

def simulate_elo(men, games):
    start_time= time()
    start_elo= { l: 1800-100*leagues.index(l) for l in leagues}
    elo= {m.idx: start_elo[main_league(m)] for m in men}  
    s_games= sorted(games, key= total_days)
    for g in s_games:
        ex= elo[g.player]
        ey= elo[g.opponent1]
        g.elo_pl= ex
        g.elo_o1= ey
        d= time_fac(g.date)*50*( g.win-p_x_wins(ex, ey))
        elo[g.player]+= d
        elo[g.opponent1]-= d
    print("this took", time()-start_time)
    return elo

def compute_elo(men, games):
    start= time()
    n= 0
    elo= {m.idx: 1000 for m in men}        
    while True:
        #compute mistake
        grad= {m.idx: 0 for m in men}
        for g in games:
            x_won= 1 if g.win else 0
            ex= elo[g.player]
            ey= elo[g.opponent1]
            grad[g.player]+=    time_fac(g.date)*10*( x_won-p_x_wins(ex, ey))
            grad[g.opponent1]+= time_fac(g.date)*10*(-x_won+p_x_wins(ex, ey))
        s= step_size(n)
        #decent
        for m in men:
            elo[m.idx]+= s*grad[m.idx]
        #abbruch
        if n > 20:
            break
        n+= 1
    print("this took", time()-start)
    return elo