from __future__ import division
import os
from numpy import array as ar
import numpy as np

def mykey(line):
    date= line.split('  ')[0].split('.')    
    return 365*int(date[2])+30*int(date[1])+int(date[0])

def sort_file(path):
    instream= open(path, 'r')
    lines= instream.readlines()
    instream.close()
    slines= sorted(lines, key=mykey)
    
    outstream= open(path, 'w')
    outstream.writelines(slines)
    outstream.close()
    
def sort_all():
    paths= ["data/"+ name + '/S' for name in os.listdir('data')]
    paths+= ["data/"+ name + '/M' for name in os.listdir('data')]
    paths+= ["data/"+ name + '/D' for name in os.listdir('data')]
    for path in paths:
        if os.path.exists(path):
            sort_file(path)