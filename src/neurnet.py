from __future__ import division
import numpy as np
from elo import *

import torch
import torch.nn as nn
import torch.nn.functional as F

class Net:
    def __init__(self):
        self.batch_size= 50
        self.model= nn.Sequential(nn.Linear(3, 6), nn.ReLU(),  nn.Linear(6, 6), nn.ReLU(), nn.Linear(6,3), nn.ReLU(), nn.Linear(3,1) )
        #TODO float is better
        self.model.double()
        self.criterion = torch.nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr = 0.01)
    
    def select_train_set(self, games, perc= .5):
        N= len(games)
        all_data= np.array([[p_x_wins(g.elo_pl, g.elo_o1), g.league_pl, g.league_op] for g in games])
        whole_truth= np.array([1 if g.win else 0 for g in games]).reshape((N,1)) 
        idx = torch.randperm(N)
        train_idx= idx[:int(N*perc)]
        test_idx= idx[int(N*perc):]
        self.train_data= all_data[train_idx]
        self.train_truth= whole_truth[train_idx]
        self.test_data= all_data[test_idx]
        self.test_games= np.array(games)[test_idx].reshape((len(test_idx),1))
        #zum schummeln/ testen, wie das netztwerk ueberhaupt lernt
#        self.test_data= all_data[train_idx]
#        self.test_games= np.array(games)[train_idx].reshape((len(train_idx),1))
        self.test_truth= whole_truth[test_idx]

    def load_batch(self):
        idx= torch.randperm(len(self.train_data))[:self.batch_size]
        return torch.tensor(self.train_data[idx]), torch.DoubleTensor(self.train_truth[idx])

    def train(self, games, epochs=1000, leorning_rate= .001, perc=.5):
        self.select_train_set(games, perc)
        for epoch in range(epochs):
            data, truth= self.load_batch()
            guess= self.model(data)
            loss= self.criterion(guess, truth)
            if epoch%100==0:
                print('epoch: ', epoch,' loss: ', loss.item())
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

    def calc_test_data(self):
#        guess= np.array( [self.model(torch.tensor(game)) for game in self.test_data] )
        guess= self.model(torch.tensor(self.test_data))
        return self.test_games, guess
        
    def save_weights(self):
        torch.save(self.model.state_dict(), "nn_weights")
    
    def load_weights(self):
        self.model.load_state_dict(torch.load("nn_weights"))
