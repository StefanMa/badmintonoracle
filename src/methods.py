import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup
import os
from numpy import array as ar
from time import localtime

import numpy as np

class Game:
    #t ist aus [D,M,S] (double, mixed, single)
    def __init__(self, html_data, t, date, klasse):
        self.game_type= t
        self.date= date
        self.klasse= klasse
        data= html_data.find_all('td')
        name1= data[1].text
        name2= data[3].text
        if t in 'MD':
            name11= name1.split('/')[0].strip()
            name12= name1.split('/')[1].strip()
            name1= name11 + " and " + name12 
            name21= name2.split('/')[0].strip()
            name22= name2.split('/')[1].strip()
            name2= name21 + " and " + name22
            self.name11= name11
            self.name12= name12
            self.name21= name21
            self.name22= name22
        else:
            self.name1= name1
            self.name2= name2
        s= name1 + " vs. " + name2 + "\n"
        set3= data[7].text
        if len(set3) > 3:
            self.setnumber= 3
        else:
            self.setnumber= 2
        self.points= np.zeros((self.setnumber, 2))
        for i in range(self.setnumber):
            self.points[i][0]= int(data[5+i].text.split(':')[0].strip())
            self.points[i][1]= int(data[5+i].text.split(':')[1].strip())  
        s+= "result: " + str(int(self.points[0][0]))+":"+ str(int(self.points[0][1]))
        for i in range(1,self.setnumber):
            s+= " || " + str(int(self.points[i][0]))+":"+ str(int(self.points[i][1]))
        s+= "   " + self.klasse
        s+= "\n"
        self.strg= s

        
    def __str__(self):
        return self.strg
        
def fill(s, n):
    full= str(s)
    for i in range(n-len(s)):
        full+=' '
    return full

def save_content(url, name, ending='.html'):
    response = urllib.request.urlopen(url)
    webContent = response.read()
    f = open(name+ending, 'wb')
    f.write(webContent)
    f.close   
    
def get_content(url):
    response = urllib.request.urlopen(url)
    webContent = response.read()
    return webContent


def make_dirs():
    if not 'data' in os.listdir():
        os.mkdir('data')

def is_in_data(game, path):
    if not os.path.exists(path):
        return False
    f= open(path, 'r')
    lines= f.readlines()
    for line in lines:
        if game.date in line:
            return True
    f.close()
    return False

l=25
def save_game(game):
    make_dirs()
    if game.game_type in 'DM':
        names= [game.name11, game.name12, game.name21, game.name22]
    else:
        names=[game.name1, game.name2]
    for name in names:
        if not name in os.listdir('data'):
            os.mkdir('data/'+name)
    if game.game_type == 'S':
        strings= [fill(game.name2, l), fill(game.name1, l)]
        place=['heim', 'ausw']
    else:
        strings= [fill(game.name12, l) + fill(game.name21, l)+'/'+fill(game.name22, l),
                  fill(game.name11, l) + fill(game.name21, l)+'/'+fill(game.name22, l),
                  fill(game.name22, l) + fill(game.name11, l)+'/'+fill(game.name12, l),
                  fill(game.name21, l) + fill(game.name11, l)+'/'+fill(game.name12, l) ]
        place=['heim', 'heim', 'ausw', 'ausw']
    for i in range(len(names)):
        if place[i]=='ausw':# fuer den gast drehen wir das ergebnis um
            res=str(int(game.points[0,1]))+":"+str(int(game.points[0,0]))
            for j in range(1, game.setnumber):
                res+= "||"+str(int(game.points[j,1]))+":"+str(int(game.points[j,0]))            
        else:
            res=str(int(game.points[0,0]))+":"+str(int(game.points[0,1]))
            for j in range(1, game.setnumber):
                res+= "||"+str(int(game.points[j,0]))+":"+str(int(game.points[j,1]))
        line= fill(game.date, 12) + strings[i] + fill(res, 22)+ fill(place[i],6)+ fill(game.klasse,6) + "\n"
        path= "data/"+names[i]+"/"+game.game_type
        if not is_in_data(game, path):
            f= open(path, 'a')
            f.write(line)
            f.close()
    
def grase(url):
    content= get_content(url)
    parsed= BeautifulSoup(content, "lxml")
    
    body= parsed.body
    if 'kampflos verloren' in str(body) or 'war falsch' in str(body):
        print("ein kampfloses Spiel")
        return
    h2_list= body.contents
    tr_list= h2_list[8].find_all('tr')
    tr= tr_list[-1]
    date=tr.contents[-2].text
    
    result= h2_list[13]
    games= result.find_all('tr')
    
    t=['D','D','D','S','M','S','S','S']
    klasse= find_out_klasse(str(body))
    make_dirs()
    for i in range(8):
        try:
            game= Game(games[i], t[i], date, klasse)
            save_game(game)
        except:
            print("Spiel konnte nicht gelesen werden")
        
def grase_table(table_url):
    content=get_content(table_url)
    parsed= BeautifulSoup(content, "lxml")
    raw_links= str(parsed).split('spielberichte-vereine/')
    links=[r.split('.HTML')[0]+'.HTML' for r in raw_links]
    links= links[1:]
    real_links=[]
    for link in links:
        if not 'verein' in link:
            real_links+= [link]
    
    parent= table_url[:table_url.rindex('/')]
    parent= table_url[:parent.rindex('/')]
    abs_links=[parent + '/spielberichte-vereine/' + link for link in real_links]
    for l in abs_links:
        try:
            grase(l)
        except:
            print("Link zu einer Begegnung funktioniert nicht")
    
def grase_aktuell():
    grase_year('aktuell')
    
def grase_year(year_str):
    idx= 1
    while idx < 30:
        try:
            idx_str= str(int(idx))
            if idx < 10:
                idx_str= '0'+idx_str
            link= 'https://www.bvbb-online.de/bvbb_daten/daten_aus_access/meisterschaft/'+year_str+'/tabellen/uebersicht-' + idx_str + '.HTML'
            print(link)
            grase_table(link)
        except :
            print("last table:" , idx-1)
            break
        idx+= 1
    
    
def grase_all():
    year= localtime()[0] - localtime()[0]//100*100
    month= localtime()[1]
    if month < 6:
        year-= 1
    first_year= 15
    
    year_list=[]
    
    for y1 in range(first_year, year):
        y2= y1+1
        year_list+= ['saison' + str(y1)+str(y2)]
            
    year_list+= ['aktuell']
    for year_str in year_list:
        grase_year(year_str)
    
def find_out_klasse(s):
    keys=['BERLIN-BRANDENBURG-LIGA', 'L A N D E S L I G A', 'B E Z I R K S K L A S S E', 'A - K L A S S E', 'B - K L A S S E', 'C - K L A S S E', 'D - K L A S S E', 'E - K L A S S E', 'F - K L A S S E', 'G - K L A S S E', 'H - K L A S S E']
    res= ['BBl', 'LaLi', 'BZ-Kl', 'A-Kl', 'B-Kl', 'C-Kl', 'D-Kl', 'E-Kl', 'F-Kl', 'G-Kl', 'H-Kl']
    for i in range(len(keys)):
        if keys[i] in s:
            return res[i]
    
    return "Buchstabensuppe"
    
    