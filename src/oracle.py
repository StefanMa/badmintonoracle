from __future__ import division
import numpy as np
from myreader import *


def interval(val, n):
    if val>=1:
        return n-1
    return int(np.floor(val*n))


class Oracle():
    def tryout(self, all_games, menschen, idx_to_name, name_to_idx, idx_to_mensch):
        
        min_num_games= 5
        n_intervals=10
        
        new_games=[]
        old_games=[]
        for g in all_games:
            if g.date[0]==20 or (g.date[0]==19 and g.date[1] > 7):
                new_games.append(g)
            else:
                old_games.append(g)
                
        #make table
        stats={} #idx->[wins, games]
        for m in menschen:
            stats[m.idx]=np.zeros((2,1))
        for g in old_games:
            stats[g.player][1]+=1
            stats[g.opponent1][1]+=1
            if g.win:
                stats[g.player][0]+= 1
            else:
                stats[g.opponent1]+= 1
        rate={}
        for m in menschen:
            if stats[m.idx][1] > 0:
                rate[m.idx]= interval(stats[m.idx][0]/stats[m.idx][1], n_intervals)
        table_raw= np.zeros((n_intervals, n_intervals, 2))
        for g in old_games:
            i,j= rate[g.player], rate[g.opponent1]
            table_raw[i,j,1]+= 1
            table_raw[i,j,0]+= g.win 
        table= table_raw[:,:,0]/table_raw[:,:,1]
        
        cases_old= np.zeros((n_intervals, 1))
        for i in range(n_intervals):
            for j in range(n_intervals):
                if np.isnan(table[i,j]):
                    continue
                case= interval(table[i,j], n_intervals)
                cases_old[case]+= table_raw[i,i,1]
        print("cases_old: ", cases_old)
        
        cases=np.zeros((n_intervals, 1))
        wins=np.zeros((n_intervals, 1))
        prediction= ar([(2*i+1)/(2*n_intervals) for i in range(n_intervals)]).reshape((n_intervals, 1))
        reality= np.zeros((n_intervals, 1))
        
        for gn in new_games:
            p_heim= idx_to_mensch[gn.player]
            p_ausw= idx_to_mensch[gn.opponent1]
            #check if both players statistic is already relevant
            past_games_heim= [g for g in p_heim.s if is_before(g.date, gn.date)]
            past_games_ausw= [g for g in p_ausw.s if is_before(g.date, gn.date)]
            if len(past_games_heim) < min_num_games-1 or len(past_games_ausw) < min_num_games-1:
                continue
            winrate_heim= sum([g.win for g in past_games_heim])/len(past_games_heim)
            winrate_ausw= sum([g.win for g in past_games_ausw])/len(past_games_ausw)
            interval_heim= interval(winrate_heim, n_intervals)
            interval_ausw= interval(winrate_ausw, n_intervals)
            chance= table[interval_heim, interval_ausw]
            if np.isnan(chance):
                continue
            case= interval(chance, n_intervals)
            cases[case]+= 1
            wins[case]+= gn.win
        #evaluation
        for i in range(n_intervals):
            reality[i]= wins[i]/cases[i]
        
        
        return table, cases, prediction, reality